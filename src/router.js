import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

let router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: () => import('@/pages/Home.vue'),
        },
        {
            path: '/product',
            name: 'Product',
            component: () => import('@/pages/Product.vue')
        },
        {
            path: '/login',
            name: 'Login',
            component: () => import('@/pages/Login.vue'),
            meta: {
                public: true
            }
        }
    ]    
})

router.beforeEach(function (to, from, next) {
    if (!to.meta.public) {
        const token = localStorage.getItem('token')
        if (token) {
            next()
        } else {
            localStorage.removeItem('user-token')
            next({ name: 'Login' })
        }
    } else {
        next()
    }
})

export default router