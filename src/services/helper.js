const digitsRE = /(\d{3})(?=\d)/g

const vIsDecimal = function (value) {
  return /^-?(?:0|0\.\d*|[1-9]\d*\.?\d*)$/.test(value)
}

const vUnmask = function (value, ds = ',') {
  return value.replace(ds, '')
}

const vMask = function (value, dp = -1, editing = false, ds = '.', gs = ',') {
  if (editing || !vIsDecimal(value)) {
    return value.replace('.', ds)
  }
  const parts = value.split(/\./)

  let dec = parts[1] || ''

  if (dp >= 0) {
    dec = dec.length < dp ? dec.padEnd(dp, '0') : dec.substr(0, dp)
  }

  if (dec) {
    dec = ds + dec
  }

  return parts[0].replace(/(\d)(?=(?:\d{3})+$)/g, '$1' + gs) + dec
}

/**
 * @author chanthou
 * @description Function get current date
 */
const vFormatSelectBox = function (arr, fText = 'name', fValue = 'id') {
  var newArr = []

  for (let item of arr) {
    var object = {}
    object.text = item[fText]
    object.value = item[fValue]
    newArr.push(object)
  }

  return newArr
}

/**
 * @author chanthou
 * @description Function convert array to hash
 */
const vConvertHash = function (arr, field = 'id') {
  let hashData = {}

  for (let item of arr) {
    hashData[item[field]] = item
  }

  return hashData
}

/**
 * @author chanthou
 * @description Function get current date
 */
const vGetCurrentDate = function () {
  let date = new Date()
  let result = vFormatDate(date)
  return result
}
/**
 * @author chanthou
 * @description Function format date yyyy/mm/ddd
 */
const vFormatDate = function (currentDate, delimiters = '-') {
  let year = currentDate.getFullYear()
  let month = ('00' + (currentDate.getMonth() + 1)).slice(-2)
  let day = ('00' + currentDate.getDate()).slice(-2)
  let result = year + delimiters + month + delimiters + day
  return result
}

const vCurrency = function (value, currency, decimals) {
  value = parseFloat(value)
  if (!isFinite(value) || (!value && value !== 0)) return ''
  currency = currency != null ? currency : '$'
  decimals = decimals != null ? decimals : 2
  var stringified = Math.abs(value).toFixed(decimals)
  var _int = decimals
    ? stringified.slice(0, -1 - decimals)
    : stringified
  var i = _int.length % 3
  var head = i > 0
    ? (_int.slice(0, i) + (_int.length > 3 ? ',' : ''))
    : ''
  var _float = decimals
    ? stringified.slice(-1 - decimals)
    : ''
  var sign = value < 0 ? '-' : ''
  return sign + currency + head +
    _int.slice(i).replace(digitsRE, '$1,') +
    _float
}

const vUniqueID = function () {
  return Math.random().toString(36).slice(2)
}

export {
  vCurrency,
  vFormatSelectBox,
  vConvertHash,
  vMask,
  vUnmask,
  vFormatDate,
  vGetCurrentDate,
  vIsDecimal,
  vUniqueID
}
