import DAO from './DAO'

const store = { 
    userInfo: {}, 
    dataList: {}, 
    message: ''
}

export default class User extends DAO {
    constructor () { 
        super('users', store)
    }

    /**
     * @author Choub Chanthou
     * @description authentication
     */
    auth (credential, callback) {
        this.axios.post('users/login', credential)
            .then(resp => {
                const data = resp.data

                if (data !== 'Unauthorized') {
                    this.store.userInfo = data.user
                    localStorage.setItem('token', data.token)
                    callback()
                }
            })
    }
}
