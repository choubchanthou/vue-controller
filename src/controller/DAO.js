'use strict'
import DB_Config from './DB_Config'
import * as _ from 'lodash'
import { Notification, Loading } from 'element-ui'

export default class DAO {

    constructor (model, store) {
        this._model = model
        this.store = store
        this.axios = DB_Config.getAxios()
    }

    /**
   * @author Choub Chanthou
   * @description get all documents for each model
   */
    getAll (callback) {
        let loadingInstance = Loading.service()
        this.axios.get(this._model + '/getall')
            .then(resp => { this.store.dataList = resp.data; callback(); setTimeout(()=> loadingInstance.close(), 1000) })
            .catch(error => { this.handleError(error); loadingInstance.close() })
    }

    /**
     * 
     * @param {*} pageNumber
     * @description get documents by pages number
     */
    getByPages (pageNumber, callback) {
        this.axios.get(this._model + '/getByPages/' + pageNumber)
            .then(resp => { this.store.dataList = resp.data; callback() })
            .catch(error => this.handleError(error))
    }

    /**
     * 
     * @param {*} JObject 
     */
    getById (id, callback) {
        this.store.item = _.find(this.store.dataList, { _id: id })

        this.axios.get(this._model + '/getById/' + id)
            .then(resp => { this.store.item = resp.data; callback()})
            .catch(error => this.handleError(error))
    }

    /**
   *
   * @param {*} JObject
   */
    create (JObject, callback) {
        const id = this.getUniqueID()
        let localOject = Object.assign({}, JObject)
        localOject['_id'] = id
        this.store.dataList.push(localOject)

        this.axios.post(this._model + '/create', JObject)
            .then(resp => {
                const index = _.findIndex(this.store.dataList, { _id: id })
                this.store.dataList.splice(index, 1, resp.data)
                callback()
                Notification({
                    title: 'Success',
                    message: 'Create new records success !',
                    type: 'success'
                })
            })
            .catch(error => { 
                Notification({
                    title: 'Error',
                    message: 'Can not create !!',
                    type: 'error'
                })
                this.removeItemById(this.store.dataList, id)
                this.handleError(error) 
            })
    }

    /**
     * 
     * @param {*} ArrayObject 
     */
    createMany (ArrayObject, callback) {
        this.axios.post(this.model + '/create/many', ArrayObject)
            .then(resp => { this.store.dataList.concat(resp.data); callback() })
            .catch(error => this.handleError(error))
    }

    /**
   * @author Choub Chanthou
   * @param {*} JObject
   */
    updateById (id, JObject, callback) {
        const index = _.findIndex(this.store.dataList, { _id: id })
        const oldObject = this.store.dataList[index]
        const localObject = Object.assign({}, JObject)
        this.store.dataList.splice(index, 1, localObject)

        this.axios.put(this._model + '/updateById/' + id, JObject)
            .then(resp => {
                Notification({
                    title: 'Success',
                    message: 'Update records success !',
                    type: 'success'
                })
                this.store.dataList.splice(index, 1, resp.data)
                callback()
            })
            .catch(error => {
                Notification({
                    title: 'Error',
                    message: 'Can not update !!',
                    type: 'error'
                })
                this.store.dataList.splice(index, 1, oldObject)
                this.axios.get(this._model + '/getById/' + id)
                    .then(resp => {
                        this.store.dataList.splice(index, 1, resp.data)
                    })
                this.handleError(error) 
            })
    }

    /**
     * @author Choub Chanthou
     * @param {*} ArrayObject 
     * @param {*} callback 
     */
    updateMany (ArrayObject, callback) {
        this.axios.put(this._model + '/updateMany', ArrayObject)
            .then(resp => callback(resp.data))
            .catch(error => this.handleError(error))
    }

    /**
     * @author Choub Chanthou
     * @param {*} fieldName 
     * @param {*} value 
     * @param {*} callback 
     */
    updateByField (fieldName, value, JObject, callback) {
        this.axios.put(this._model + '/updateByField/' + fieldName + '/' + value, JObject)
            .then(resp => callback(resp.data))
            .catch(error => this.handleError(error))
    }

    /**
     * 
     * @param {*} fieldName  
     * @param {*} value 
     * @param {*} ArrayObject 
     * @param {*} callback 
     */
    updateManyByField (fieldName, value, ArrayObject, callback) {
        this.axios.put(this._model + '/updateManyByField/' + fieldName + '/' + value, ArrayObject)
            .then(resp => callback(resp.data))
            .catch(error => this.handleError(error))
    }

    /**
   * @author Choub Chanthou
   * @param {*} id
   */
    deleteById (id, callback) {
        const item = _.find(this.store.dataList, { _id: id })
        this.removeItemById(this.store.dataList, id)

        this.axios.delete(this._model + '/deleteById/' + id)
            .then(()=> callback())
            .catch(error => { 
                Notification({
                    title: 'Error',
                    message: 'Can not delete !!',
                    type: 'error'
                })
                this.store.dataList.push(item)
                this.handleError(error) 
            })
    }

    /**
     * 
     * @param {*} error 
     */
    handleError (error) {
        try {
            const status = error.response.status

            if (status === 401) {
                window.location = '/login'
                localStorage.removeItem('token')
            } else {
                // eslint-disable-next-line no-console
                console.log(error)
            }
        } catch (error) {
            // eslint-disable-next-line no-console
            console.log(error)
        }
    }

    /**
     * 
     * @param {*} dataList 
     * @param {*} id 
     */
    removeItemById (dataList, id) {
        const index = _.findIndex(dataList, {'_id': id})

        if (index != -1) {
            dataList.splice(index, 1)
        }
    }

    /**
     * @author Choub Chanthou
     */
    getUniqueID () {
        return Math.random().toString(36).slice(2)
    }

    /**
     * 
     * @param {*} model 
     */
    setModel (model) {
        this._model = model
    }

    /**
     * @description 
     */
    getModel () {
        return this._model
    }
}
