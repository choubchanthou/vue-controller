'use strict'
import axios from 'axios'

class DB_Config{
    axios
    
    constructor() {
        this.axios = axios.create(
            {
                baseURL: 'https://express-application-framework.herokuapp.com/api/'
            }
        )

        // Add a response interceptor
        this.axios.interceptors.response.use(
            function (response) {
                return response
            },
            function (error) {
                return Promise.reject(error)
            }
        )
    }

    getAxios () {
        const token = localStorage.getItem('token') || ''
        this.axios.defaults.headers.common['x-auth'] = token

        return this.axios
    }
}

export default new DB_Config()