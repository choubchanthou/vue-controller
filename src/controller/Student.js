import DAO from './DAO'

const store = { 
    dataList: {},
    item: {}
}

export default class Student extends DAO {
    constructor () { 
        super('student', store)
    }
}
