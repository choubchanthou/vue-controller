# Vue Controller 

###Install
To install this package you will need:

* element-ui: ^2.4.11
* axios : ^0.18.0
* lodash : ^4.17.11

Install Vue Controller.
```
Copy folder <controller> put into <src>
```
###File structure

```bash
.
├── DAO.js
├── DB_Config.js
├── Student.js
├── User.js
└── State.js
```

### Common DAO important:

* getAll ()
* getByPages (pageNumber)
* getById (id)
* create (JObject)
* deleteById (id)
* updateById (id, JObject)

### Implementation

* Create Sub Controller
```
//create Student.js
import DAO from './DAO'

const store = { 
    dataList: {},
    item: {}
}

export default class Student extends DAO {
    constructor () { 
        super('student', store)
    }
}
```
* Call controller in Components
```
<template>
    <div class="home">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col"><button class="btn btn-success btn-sm" @click="showDailog()">Create</button></th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(student, index) in studentCtrl.store.dataList" :key="index">
                    <th scope="row">{{ index + 1 }}</th>
                    <td>{{ student.name }}</td>
                    <td>{{ student.sex }}</td>
                    <td><button class="btn btn-danger btn-sm" @click="deleteById(student._id)">Delete</button> || <button class="btn btn-danger btn-sm" @click="getStudentById(student._id)">Edit</button></td>
                </tr>
            </tbody>
        </table>
    </div>
</template>

<script>
import Student from "@/controller/Student"
export default {
    data() {
        studentCtrl: { }
    },
    mounted() { 
        this.studentCtrl = new Student() 
        this.studentCtrl.getAll()
    },
    methods: {
        deleteById (id) {
            this.studentCtrl.deleteById(id)
        }, 
        getStudentById (id) {
            this.studentCtrl.getById(id)
        },
        createStudent () {
            const student = {
                name: 'Chanthou',
                description: 'IT'
            }
            this.studentCtrl.create(student)
        },
        updateStudent () {
            const student = this.studentCtrl.store.item
            this.studentCtrl.updateById(student._id, student)
        }
    }     

}

</script>
```







